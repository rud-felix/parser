<?php

// Service definitions
$container = \App\Core\Service\ServiceContainer::getInstance();

// TODO: try to use Reflection API for generation this definition
$container->csvWriter = function () {
    // TODO:  add into the class method createInstance()
    return new \App\Parser\Writer\CsvWriter();
};

$container->csvReader = function () {
    return new \App\ReportViewer\Reader\CsvReader();
};

$container->htmlProvider = function () {
    return new \App\Parser\Provider\HtmlProvider();
};

$container->parser = function ($container) {
    return new App\Parser\Parser($container->csvWriter, $container->htmlProvider);
};

$container->helper = function () {
    return new App\Helper\Helper();
};

$container->reportViewer = function ($container) {
    return new \App\ReportViewer\ReportViewer($container->csvReader);
};
