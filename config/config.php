<?php

return [
    'modules' => [
        'parser' => [
            'description' => 'Parse information from getting url and save result about it',
            'command' => [
                'name' => 'parse',
                'class' => 'App\Parser\Command\ParseCommand'
            ],
            'strategy' => [
                'App\Parser\Strategy\TagTextLengthStrategy' => [
                    'tags' => [
                        'App\Parser\Tag\Title',
                        'App\Parser\Tag\Description',
                        'App\Parser\Tag\H1',
                    ],
                    'report' => [
                        'columnTitle' => 'Text length'
                    ]
                ],
                'App\Parser\Strategy\TagCountStrategy' => [
                    'tags' => [
                        'App\Parser\Tag\Img',
                        'App\Parser\Tag\A',
                    ],
                    'report' => [
                        'columnTitle' => 'Count'
                    ]
                ],
                'App\Parser\Strategy\TagCountCharactersStrategy' => [
                    'tags' => [
                        'App\Parser\Tag\P',
                    ],
                    'report' => [
                        'columnTitle' => 'Count characters'
                    ]
                ],
                'App\Parser\Strategy\TagCountCharactersWithoutSpacesStrategy' => [
                    'tags' => [
                        'App\Parser\Tag\P',
                    ],
                    'report' => [
                        'columnTitle' => 'Count characters without spaces'
                    ]
                ]
            ],

            /* Additional strategy which runs after all prev strategies */
            'additionalStrategy' => [
                'App\Parser\Strategy\SiteMapStrategy' => [
                    'report' => [
                        'columnTitle' => 'Sitemap'
                    ]
                ],
            ]
        ],

        'reporter' => [
            'description' => 'Print information about analyzed domain',
            'command' => [
                'name' => 'report',
                'class' => 'App\ReportViewer\Command\ReportCommand'
            ],
            'rowFormat' => [
                'pipe' => "\e[32m|\e[0m",
                'formatt' => '{$pipe} %-100s {$pipe}%6s {$pipe}%6s {$pipe}%6s {$pipe}%6s {$pipe}%6s {$pipe}%6s {$pipe}%6s {$pipe}%6s',
            ]
        ],

        'helper' => [
            'description' => 'Print information about all command in app',
            'command' => [
                'name' => 'help',
                'class' => 'App\Helper\Command\HelpCommand'
            ],
        ]
    ]
];
