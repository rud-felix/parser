<?php

require_once __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config/services.php';

// Change php.ini properties
set_time_limit(0);
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', true);
