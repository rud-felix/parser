<?php

namespace App\Parser;

use App\Core\ValueObject\Url;
use App\Parser\Provider\DataProviderInterface;
use App\Parser\Strategy\StrategyInterface;
use App\Config;
use App\Parser\Writer\ReportWriterInterface;

class Parser
{
    /**
     * @var ReportWriterInterface
     */
    private $writer;

    /**
     * @var DataProviderInterface
     */
    private $dataProvider;

    /**
     * @param ReportWriterInterface $writer
     * @param DataProviderInterface $dataProvider
     */
    public function __construct(ReportWriterInterface $writer, DataProviderInterface $dataProvider)
    {
        $this->writer = $writer;
        $this->dataProvider = $dataProvider;
    }

    /**
     * @param Url $url
     */
    public function parse(Url $url): void
    {
        $result = $this->parseProcess($url);
        $result = $this->analyzeResult($url, $result);

        $this->writer->createFile($url->getDomain());
        $this->writer->write($result);

        $this->printParseResult();
    }

    /**
     * @param Url $url
     * @param array $result
     * @return array
     */
    private function parseProcess(Url $url, array $result = []): array
    {
        static $count;
        $count++;
        echo $count . ' - ' . $url->getUrl() . PHP_EOL; // TODO: replace echo => fputs

        $content = $this->getContent($url);
        $result[$url->getUrl()] = $this->analyzeContent($content);

        $otherLinks = $this->findAllLinks($content, $url->getDomain());
        $otherLinks = $this->filterLinks($otherLinks);
        $result = $this->analyzeOtherLinks($result, $otherLinks, $url);

        return $result;
    }

    /**
     * @param array $links
     * @return array
     */
    private function filterLinks(array $links): array
    {
        return array_unique($links);
    }

    /**
     * @param string $content
     * @return array
     */
    private function analyzeContent(string $content): array
    {
        $result = [];
        $strategies = $this->getAllStrategyByModule();

        /** @var StrategyInterface $strategy */
        foreach ($strategies as $strategyClass => $strategyDetails) {
            // TODO use DIC for getting strategy instance
            $result[$strategyClass] = (new $strategyClass())
                ->analyze(['content' => $content]);
        }

        return $result;
    }

    /**
     * @param string $content
     * @param string $domain
     * @return mixed
     */
    private function findAllLinks(string $content, string $domain)
    {
        $pattern = "/(?!.*(png|jpg|gif))<a.*?href=[\"\\']?((https?):\\/\\/(www\\.)?{$domain}[^\"\\'>?|]+)[\"\\']?/i";
        preg_match_all($pattern, $content, $matches);

        return $matches[2];
    }

    /**
     * @param array $result
     * @param array $otherLinks
     * @param Url $url
     * @return array
     */
    private function analyzeOtherLinks(array $result, array $otherLinks, Url $url): array
    {
        foreach ($otherLinks as $link) {
            // TODO: refactor
            $pattern = "/((https?):\\/\\/(www\\.)?{$url->getDomain()})/i";
            $link = preg_match($pattern, $link)
                ? new Url($link)
                : new Url($url->getDomain() . $link);

            if ($this->isUrlAnalyzed($link, $result) === true) {
                continue;
            }

            $result = $this->parseProcess($link, $result);
        }

        return $result;
    }

    /**
     * @param Url $url
     * @param array $result
     * @return bool
     */
    private function isUrlAnalyzed(Url $url, array $result): bool
    {
        foreach ($result as $key => $value) {
            if ($key === $url->getUrl()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Url $url
     * @return string
     */
    private function getContent(Url $url): string
    {
        return $this->dataProvider->getDataByUrl($url);
    }

    /**
     * @param string $module
     * @return array
     */
    private function getAllStrategyByModule($module = 'parser'): array
    {
        return Config::getModuleStrategies($module);
    }

    /**
     * @param Url $url
     * @param array $parseResult
     * @return array
     */
    private function analyzeResult(Url $url, array $parseResult): array
    {
        $result = [];
        $additionalStrategies = $this->getAdditionalModuleStrategy();

        /** @var StrategyInterface $strategyClass */
        foreach ($additionalStrategies as $strategyClass => $details) {
            // TODO use DIC for getting strategy instance
            $result = (new $strategyClass())
                ->analyze(['parseResult' => $parseResult, 'url' => $url]);
        }

        return $result;
    }

    /**
     * @param string $module
     * @return array
     */
    private function getAdditionalModuleStrategy($module = 'parser'): array
    {
        $properties = Config::getModuleProperties($module);

        return $properties['additionalStrategy'];
    }

    /**
     * Print in stdout parse result information
     */
    private function printParseResult(): void
    {
        echo PHP_EOL . "\e[32m Report file: \e[0m" . $this->writer->getFilePath() . PHP_EOL;
    }
}
