<?php

namespace App\Parser\Tag;

class Img implements TagInterface
{
    /**
     * @return string
     */
    public static function getTagName(): string
    {
        return 'img';
    }
}
