<?php

namespace App\Parser\Tag;

class Description implements TagInterface
{
    /**
     * @return string
     */
    public static function getTagName(): string
    {
        return 'description';
    }
}
