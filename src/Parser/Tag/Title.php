<?php

namespace App\Parser\Tag;

class Title implements TagInterface
{
    /**
     * @return string
     */
    public static function getTagName(): string
    {
        return 'title';
    }
}
