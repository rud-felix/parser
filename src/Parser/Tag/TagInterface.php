<?php

namespace App\Parser\Tag;

interface TagInterface
{
    /**
     * @return string
     */
    public static function getTagName(): string;
}
