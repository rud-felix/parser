<?php

namespace App\Parser\Tag;

class H1 implements TagInterface
{
    /**
     * @return string
     */
    public static function getTagName(): string
    {
        return 'h1';
    }
}
