<?php

namespace App\Parser\Tag;

class A implements TagInterface
{
    /**
     * @return string
     */
    public static function getTagName(): string
    {
        return 'a';
    }
}
