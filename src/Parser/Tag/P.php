<?php

namespace App\Parser\Tag;

class P implements TagInterface
{
    /**
     * @return string
     */
    public static function getTagName(): string
    {
        return 'p';
    }
}
