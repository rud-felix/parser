<?php

namespace App\Parser\Command;

use App\Core\Interfaces\CommandInterface;
use App\Core\Service\ServiceContainer;
use App\Core\ValueObject\Url;

class ParseCommand implements CommandInterface
{
    /**
     * @param array $argv
     */
    public static function execute(array $argv): void
    {
        $url = $argv[2];
        $url = new Url($url);

        $container = self::getContainer();
        $container->parser->parse($url);
    }

    /**
     * @return ServiceContainer
     */
    private static function getContainer(): ServiceContainer
    {
        return ServiceContainer::getInstance();
    }
}
