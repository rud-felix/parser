<?php

namespace App\Parser\Strategy;

use App\Parser\Tag\TagInterface;
use App\Config;

class TagTextLengthStrategy implements StrategyInterface
{
    /**
     * @param array $params
     * @return array
     */
    public function analyze(array $params): array
    {
        $result = [];
        $tags = $this->getTagList();

        /** @var TagInterface $tag */
        foreach ($tags as $tag) {
            $tagValue = $this->getTagValue($params['content'], $tag::getTagName());
            $result[$tag::getTagName()] = strlen($tagValue);
        }

        return $result;
    }

    /**
     * @return array
     */
    private function getTagList(): array
    {
        return Config::getStrategyTags('parser', __CLASS__); // TODO: fix hardcode
    }

    /**
     * @param string $content
     * @param string $tag
     * @return string
     */
    private function getTagValue(string $content, string $tag)
    {
        $pattern = "/<{$tag}>(.*?)<\\/{$tag}>/i";
        preg_match($pattern, $content, $matches);
        $extractedText = $matches[1] ?? '';

        return $this->filterString($extractedText);
    }

    /**
     * @param string $str
     * @return string
     */
    private function filterString(string $str): string
    {
        return htmlentities($str);
    }
}
