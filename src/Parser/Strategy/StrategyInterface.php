<?php

namespace App\Parser\Strategy;

interface StrategyInterface
{
    /**
     * @param array $params
     * @return array
     */
    public function analyze(array $params): array;
}
