<?php

namespace App\Parser\Strategy;

use App\Parser\Tag\TagInterface;
use App\Config;

class TagCountCharactersStrategy implements StrategyInterface
{
    /**
     * @param array $params
     * @return array
     */
    public function analyze(array $params): array
    {
        $result = [];
        $tags = $this->getTagList();
        /** @var TagInterface $tag */
        foreach ($tags as $tag) {
            $result[$tag::getTagName()] = $this->getTagCountCharactersWithSpaces($params['content'], $tag::getTagName());
        }

        return $result;
    }

    /**
     * @return array
     */
    private function getTagList(): array // TODO: DRY for this function
    {
        return Config::getStrategyTags('parser', __CLASS__); // TODO: fix hardcode
    }

    /**
     * @param string $content
     * @param string $tag
     * @return int
     */
    private function getTagCountCharactersWithSpaces(string $content, string $tag): int
    {
        $pattern = "/<{$tag}>(.*?)<\\/{$tag}>/i";
        preg_match_all($pattern, $content, $matches);

        $count = 0;
        foreach ($matches[1] as $match) {
            $count += strlen($match);
        }

        return $count;
    }
}
