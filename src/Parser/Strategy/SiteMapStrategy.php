<?php

namespace App\Parser\Strategy;

use App\Core\Exception\RemoteFileNotExistException;
use App\Core\ValueObject\Url;
use \Throwable;

class SiteMapStrategy implements StrategyInterface
{
    /**
     * @param array $params
     * @return array
     */
    public function analyze(array $params): array
    {
        $parseResult = $params['parseResult'];
        $url = $params['url'];

        $result = $this->analyzeProcess($url, $parseResult, true);

        return $result;
    }

    /**
     * @param Url $url
     * @param array $parseResult
     * @param bool $defaultSiteMapPath
     * @return array
     */
    private function analyzeProcess(Url $url, array $parseResult, bool $defaultSiteMapPath): array
    {
        $siteMapFile = $this->getSiteMapFile($url, $defaultSiteMapPath);
        $nested = $this->isSiteMapHaveNestedSitemap($siteMapFile);
        $siteMapLinks = $this->getSiteMapLinks($siteMapFile);

        // TODO: need refactor this part
        if (count($nested)) {
            $result = $this->analyzeNestedSiteMapWithResult($siteMapLinks, $parseResult);
        } else {
            $result = $this->compareLinks($siteMapLinks, $parseResult);
        }

        return $result;
    }

    /**
     * @param array $nested
     * @param array $parseResult
     * @return array
     */
    private function analyzeNestedSiteMapWithResult(array $nested, array $parseResult): array
    {
        foreach ($nested as $link) {
            $parseResult = $this->analyzeProcess(new Url($link), $parseResult, false);
        }

        return $parseResult;
    }

    /**
     * @param array $siteMap
     * @param array $parseResult
     * @return array
     */
    private function compareLinks(array $siteMap, array $parseResult): array
    {
        foreach ($parseResult as $link => $details) {
            if (in_array($link, $siteMap)) {
                $parseResult[$link][__CLASS__] = 'true';
                continue;
            }

            $parseResult[$link][__CLASS__] = 'false';
        }

        return $parseResult;
    }

    /**
     * @param Url $url
     * @param bool $defaultFile
     * @return string
     * @throws RemoteFileNotExistException
     */
    private function getSiteMapFile(Url $url, bool $defaultFile = true): string
    {
        $path = $defaultFile
            ? $url->getScheme() . '://' . $url->getDomain() . '/sitemap.xml'
            : $url->getUrl();

        try {
            $content = file_get_contents($path);
        } catch (Throwable $e) {
            throw new RemoteFileNotExistException();
        }

        return $content;
    }

    /**
     * @param string $xml
     * @return mixed
     */
    private function isSiteMapHaveNestedSitemap(string $xml): array
    {
        $pattern = "/<sitemap>(.*)?<\\/sitemap>/i";
        preg_match_all($pattern, $xml, $matches);

        return $matches[1];
    }

    /**
     * @param string $xml
     * @return mixed
     */
    private function getSiteMapLinks(string $xml): array
    {
        $pattern = "/<loc>(.*)?<\\/loc>/i";
        preg_match_all($pattern, $xml, $matches);
        $links = $this->filterLinks($matches[1]);

        return $links;
    }

    /**
     * @param array $links
     * @return array
     */
    private function filterLinks(array $links): array
    {
        return array_map(function($link){
            return preg_replace('/([^:])(\/{2,})/', '$1/', $link);
        }, $links);
    }
}
