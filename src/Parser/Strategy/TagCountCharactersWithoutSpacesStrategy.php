<?php

namespace App\Parser\Strategy;

use App\Parser\Tag\TagInterface;
use App\Config;

class TagCountCharactersWithoutSpacesStrategy implements StrategyInterface
{
    /**
     * @param array $params
     * @return array
     */
    public function analyze(array $params): array
    {
        $result = [];
        $tags = $this->getTagList();
        /** @var TagInterface $tag */
        foreach ($tags as $tag) {
            $result[$tag::getTagName()] = $this->getTagCountCharactersWithoutSpaces(
                $params['content'],
                $tag::getTagName()
            );
        }

        return $result;
    }

    /**
     * @return array
     */
    private function getTagList(): array // TODO: DRY
    {
        return Config::getStrategyTags('parser', __CLASS__); // TODO: fix hardcode
    }

    /**
     * @param string $content
     * @param string $tag
     * @return int
     */
    private function getTagCountCharactersWithoutSpaces(string $content, string $tag): int
    {
        $pattern = "/<{$tag}>(.*?)<\\/{$tag}>/i";
        preg_match_all($pattern, $content, $matches);

        $count = 0;
        foreach ($matches[1] as $match) {
            $count += strlen($this->removeAllWhiteSpaces($match));
        }

        return $count;
    }

    /**
     * @param string $str
     * @return string
     */
    private function removeAllWhiteSpaces(string $str): string
    {
        return preg_replace('/\s/', '', $str);
    }
}
