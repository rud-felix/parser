<?php

namespace App\Parser\Strategy;

use App\Parser\Tag\TagInterface;
use App\Config;

class TagCountStrategy implements StrategyInterface
{
    /**
     * @param array $params
     * @return array
     */
    public function analyze(array $params): array
    {
        $result = [];
        $tags = $this->getTagList();
        /** @var TagInterface $tag */
        foreach ($tags as $tag) {
            $result[$tag::getTagName()] = $this->getTagCount($params['content'], $tag::getTagName());
        }

        return $result;
    }

    /**
     * @return array
     */
    private function getTagList(): array // TODO: DRY
    {
        return Config::getStrategyTags('parser', __CLASS__); // TODO: fix hardcode
    }

    /**
     * @param string $content
     * @param string $tag
     * @return int
     */
    private function getTagCount(string $content, string $tag): int
    {
        $pattern = "/<{$tag}\\s?/i";
        preg_match_all($pattern, $content, $matches);

        return count($matches[0]);
    }
}
