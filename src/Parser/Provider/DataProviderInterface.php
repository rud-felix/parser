<?php

namespace App\Parser\Provider;

use App\Core\ValueObject\Url;

interface DataProviderInterface
{
    public function getDataByUrl(Url $url);
}
