<?php

namespace App\Parser\Provider;

use App\Core\ValueObject\Url;

class HtmlProvider implements DataProviderInterface
{
    /**
     * @param Url $url
     * @return string
     */
    public function getDataByUrl(Url $url): string
    {
        return file_get_contents($url->getUrl());
    }
}
