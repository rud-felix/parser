<?php

namespace App\Parser\Writer;


interface ReportWriterInterface
{
    /**
     * @param string $filename
     * @return mixed
     */
    public function createFile(string $filename);
}
