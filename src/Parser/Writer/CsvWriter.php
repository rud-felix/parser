<?php

namespace App\Parser\Writer;

use App\Core\Traits\CsvFileUtilTrait;
use App\Core\Traits\FileUtilTrait;

class CsvWriter implements ReportWriterInterface
{
    use FileUtilTrait;
    use CsvFileUtilTrait;

    /**
     * @var string
     */
    private $filePath;

    /**
     * @param array $list
     */
    public function write(array $list): void
    {
        // TODO: need refactor this
        foreach ($list as $url => $strategies) {
            $row = [];
            $header = [];
            $header[] = ['url'];
            $row[] = $url;

            foreach ($strategies as $strategy => $tags) {
                if (is_array($tags)) {
                    foreach ($tags as $tag => $value) {
                        $header[] = $tag;
                        $row[] = $value;
                    }
                } else {
                    $row[] = $tags;
                }
            }

            $this->fputcsv($row);
        }

        $this->fclose();
    }

    /**
     * @param string $filename
     */
    public function createFile(string $filename): void
    {
        $this->filePath = getcwd() . DIRECTORY_SEPARATOR . 'report' . DIRECTORY_SEPARATOR . $filename . '.csv'; // TODO put into config
        $this->fopen($this->filePath, 'w');
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }
}
