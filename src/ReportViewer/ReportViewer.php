<?php

namespace App\ReportViewer;

use App\Core\Exception\ReportFileNotExistException;
use App\Core\Traits\CsvFileUtilTrait;
use App\Core\Traits\FileUtilTrait;
use App\Core\ValueObject\Url;
use App\ReportViewer\Reader\ReportReaderInterface;

class ReportViewer
{
    use FileUtilTrait;
    use CsvFileUtilTrait;

    /**
     * @var ReportReaderInterface
     */
    private $reader;

    /**
     * @param ReportReaderInterface $reader
     */
    public function __construct(ReportReaderInterface $reader)
    {
        $this->reader = $reader;
    }

    /**
     * @param Url $url
     */
    public function viewByUrl(Url $url)
    {
        $pathToReport = $this->pathToReportFile($url);
        $this->checkIsFileExist($pathToReport);
        $this->reader->read($pathToReport);
        $this->printToStdOut();
    }

    /**
     * Print each row from file into stdout
     */
    private function printToStdOut(): void
    {
        $this->fopen('php://output', 'w');

        foreach ($this->reader->getRows() as $row) {
            if (!$row) {
                break;
            }
            $this->printRowToStdOut($row);
        }

        $this->fclose();
    }

    /**
     * @param Url $url
     * @return string
     */
    private function pathToReportFile(Url $url): string
    {
        return getcwd() . DIRECTORY_SEPARATOR . 'report' . DIRECTORY_SEPARATOR . $url->getDomain() . '.csv'; // TODO fix
    }

    /**
     * @param $file
     * @throws ReportFileNotExistException
     */
    private function checkIsFileExist($file)
    {
        if (!file_exists($file)) {
            throw new ReportFileNotExistException();
        }
    }

    /**
     * @param array $row
     */
    private function printRowToStdOut(array $row)
    {
        // TODO: gets this from config
        $pipe = "\e[32m|\e[0m";
        $format = "{$pipe} %-90s {$pipe}%6s {$pipe}%6s {$pipe}%6s {$pipe}%6s {$pipe}%6s {$pipe}%6s {$pipe}%6s {$pipe}%6s" . PHP_EOL;
        $this->vfprintf($format, $row);
    }
}
