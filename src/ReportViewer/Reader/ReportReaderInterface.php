<?php

namespace App\ReportViewer\Reader;

interface ReportReaderInterface
{
    /**
     * @param string $pathToFile
     * @return mixed
     */
    public function read(string $pathToFile);
}
