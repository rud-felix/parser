<?php

namespace App\ReportViewer\Reader;

use App\Core\Traits\CsvFileUtilTrait;
use App\Core\Traits\FileUtilTrait;
use \Generator;

class CsvReader implements ReportReaderInterface
{
    use FileUtilTrait;
    use CsvFileUtilTrait;

    public function read(string $pathToFile)
    {
        $this->fopen($pathToFile, 'r');
    }

    public function getRows(): Generator
    {
        while (!feof($this->resource)) {
            $row = fgetcsv($this->resource, 4096);

            yield $row;
        }
        $this->fclose();

        return;
    }
}
