<?php

namespace App\ReportViewer\Command;

use App\Core\Interfaces\CommandInterface;
use App\Core\Service\ServiceContainer;
use App\Core\ValueObject\Url;

class ReportCommand implements CommandInterface
{
    public static function execute(array $argv): void
    {
        $url = $argv[2];
        $url = new Url($url);

        $container = self::getContainer();
        $reportViewer = $container->reportViewer;
        $reportViewer->viewByUrl($url);
    }

    /**
     * @return ServiceContainer
     */
    private static function getContainer(): ServiceContainer
    {
        return ServiceContainer::getInstance();
    }
}
