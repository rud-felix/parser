<?php

namespace App\Core\Service;

final class ServiceContainer
{
    /**
     * @var array
     */
    private $storage;

    /**
     * @var ServiceContainer
     */
    private static $instance;

    /**
     * Gets instance
     */
    public static function getInstance(): ServiceContainer
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * prev creating multiple instances
     */
    private function __construct()
    {
    }

    /**
     * prev instance from clone
     */
    private function __clone()
    {
    }

    /**
     * prev instance from being unserialized
     */
    private function __wakeup()
    {
    }

    /**
     * @param string $name
     * @param $object
     */
    public function __set(string $name, $object)
    {
        $this->storage[$name] = $object;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->storage[$name]($this);
    }
}
