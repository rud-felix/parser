<?php

namespace App\Core\Traits;

trait CsvFileUtilTrait
{
    /**
     * @var resource
     */
    private $resource;

    public function fputcsv($row): void
    {
        fputcsv($this->resource, $row);
    }

    /**
     * @return array
     */
    public function fgetcsv($resource): array
    {
        return fgetcsv($resource, 4096);
    }

    public function vfprintf(string $format, array $row): void
    {
        vfprintf($this->resource, $format, $row);
    }
}