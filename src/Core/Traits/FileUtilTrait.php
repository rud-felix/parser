<?php

namespace App\Core\Traits;

trait FileUtilTrait
{
    /**
     * @var resource
     */
    private $resource;

    /**
     * @param string $path
     * @param string $mode
     */
    public function fopen(string $path, string $mode): void
    {
        $this->resource = fopen($path, $mode);
    }

    public function fclose(): void
    {
        fclose($this->resource);
    }

}