<?php

namespace App\Core\Traits;

trait MemoryUtilTrait
{
    /**
     * Source http://php.net/manual/en/function.memory-get-usage.php
     *
     * @param $size
     * @return string
     */
    function convert($size)
    {
        $unit=array('b','kb','mb','gb','tb','pb');
        return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
    }
}
