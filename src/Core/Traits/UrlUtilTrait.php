<?php

namespace App\Core\Traits;

trait UrlUtilTrait
{
    /**
     * @param $url
     * @return string
     */
    function addHttpProtocol($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }
}