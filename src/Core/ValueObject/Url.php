<?php

namespace App\Core\ValueObject;

use App\Core\Exception\ValueObjectIsNotValidException;
use App\Core\Traits\UrlUtilTrait;

class Url
{
    use UrlUtilTrait;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var string
     */
    private $scheme;

    /**
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = $this->addHttpProtocol($url);
        $this->filter();
        $this->isValid();
        $this->parse();
    }

    private function filter(): void
    {
        $this->url = trim($this->url);
    }

    private function parse(): void
    {
        $this->domain = parse_url($this->url, PHP_URL_HOST);
        $this->scheme = parse_url($this->url, PHP_URL_SCHEME);
    }

    /**
     * @throws ValueObjectIsNotValidException
     */
    private function isValid(): void
    {
        // TODO: replace on regexp
        if (filter_var($this->url, FILTER_VALIDATE_URL) === false) {
            throw new ValueObjectIsNotValidException();
        }
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @return string
     */
    public function getScheme(): string
    {
        return $this->scheme;
    }
}
