<?php

namespace App\Core\Exception;

use \Exception;

class RemoteFileNotExistException extends Exception
{
    protected $message = 'Remote file does not exist';
}
