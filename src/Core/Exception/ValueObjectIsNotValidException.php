<?php

namespace App\Core\Exception;

use \Exception;

class ValueObjectIsNotValidException extends Exception
{
    protected $message = 'Data into value object is not valid';
}
