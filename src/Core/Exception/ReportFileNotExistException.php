<?php

namespace App\Core\Exception;

use \Exception;

class ReportFileNotExistException extends Exception
{
    protected $message = 'Report file not exist';
}
