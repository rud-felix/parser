<?php

namespace App\Core\Interfaces;

interface CommandInterface
{
    /**
     * @param array $argv
     */
    public static function execute(array $argv): void;
}
