<?php

namespace App;

class Config
{
    /**
     * @var array
     */
    private static $params;

    /**
     * @param array $params
     */
    public function __construct(array $params)
    {
        self::$params = $params;
    }

    /**
     * @return array
     */
    public static function getModules(): array
    {
        return self::$params['modules'];
    }

    /**
     * @param string $moduleName
     * @return array
     */
    public static function getModuleStrategies(string $moduleName): array
    {
        return self::$params['modules'][$moduleName]['strategy'];
    }

    /**
     * @param string $moduleName
     * @return array
     */
    public static function getModuleProperties(string $moduleName): array
    {
        return self::$params['modules'][$moduleName];
    }

    /**
     * @param string $moduleName
     * @param string $strategy
     * @return array
     */
    public static function getStrategyTags(string $moduleName, string $strategy): array
    {
        return self::$params['modules'][$moduleName]['strategy'][$strategy]['tags'];
    }

    /**
     * @return array
     */
    public static function getCommands(): array
    {
        $commands = [];
        $modules = self::$params['modules'];
        foreach ($modules as $moduleName => $properties) {
            $commands[$properties['command']['name']] = $properties['command'];
            $commands[$properties['command']['name']]['description'] = $properties['description'];
        }

        return $commands;
    }

    /**
     * @param string $moduleName
     * @param string $strategy
     * @return string
     */
    public static function getStrategyReportTitle(string $moduleName, string $strategy): string
    {
        return self::$params['modules'][$moduleName]['strategy'][$strategy]['report']['columnTitle'];
    }
}
