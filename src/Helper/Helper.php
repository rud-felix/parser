<?php

namespace App\Helper;

use App\Config;

class Helper
{
    public function printAllCommands(): void
    {
        $commands = $this->getAllCommands();
        $this->printInStdOut($commands);
    }

    private function printInStdOut(array $commands): void
    {
        $stdout = $this->openStdOut();

        foreach ($commands as $command) {
            $line = "\e[32m {$command['name']} \033[0m - {$command['description']}\n";
            fputs($stdout, $line);
        }
    }

    /**
     * @return array
     */
    private function getAllCommands(): array
    {
        return Config::getCommands();
    }

    /**
     * @return bool|resource
     */
    private function openStdOut()
    {
        return fopen('php://stdout', 'w');
    }
}
