<?php

namespace App\Helper\Command;

use App\Core\Interfaces\CommandInterface;
use App\Core\Service\ServiceContainer;

class HelpCommand implements CommandInterface
{
    /**
     * @param array $argv
     */
    public static function execute(array $argv): void
    {
        $container = self::getContainer();
        $helper = $container->helper;
        $helper->printAllCommands();
    }

    /**
     * @return ServiceContainer
     */
    private static function getContainer(): ServiceContainer
    {
        return ServiceContainer::getInstance();
    }
}
