<?php

namespace App;

class CommandSwitcher
{
    /**
     * @param array $argv
     * @param array $config
     */
    public static function switch(array $argv, array $config): void
    {
        self::populateConfig($config);

        $command = $argv[1];
        $modules = Config::getModules();
        self::executeClassByCommand($command, $modules, $argv);
    }

    /**
     * @param string $parserCommand
     * @param array $modules
     * @param array $argv
     */
    private static function executeClassByCommand(string $parserCommand, array $modules, array $argv): void
    {
        foreach ($modules as $module => $properties) {
            $command = $properties['command'];
            if ($command['name'] == $parserCommand) {
                $command['class']::execute($argv); // TODO: try to use ...$argv
            }
        }
    }

    /**
     * @param $params
     */
    private static function populateConfig($params): void
    {
        new Config($params);
    }
}
