# Command Line Parser

### Configuring
```sh
$ composer install
```

### Commands

```sh
$ parser help
```
```sh
$ parser parse example.com
```
```sh
$ parser report example.com
```

### Config
In `config/config.php` you can 
- add parsing strategies
- change command name
- change command description
- etc...

In `config/services.php` you can set dependency 